# lkcamp_presentations

Slide and other materials used for the meeting presentations

## Dependencies:
### Debian installation:
```bash
$ sudo apt install texlive-latex-extra ttf-dejavu texlive-fonts-extra xzdec
$ tlmgr init-usertree
$ # use an older repository if running Ubuntu 18.04
$ [ "`lsb_release -r | awk '{ print $2 }'`" == "18.04" ] && tlmgr option repository ftp://tug.org/historic/systems/texlive/2017/tlnet-final
$ tlmgr install dsfont carlito
$ updmap --user #update maps
```

## Build
Just calling make will build all presentations. You can also build
just an specific presentation by calling:
```bash
$ make <project-name>
```
